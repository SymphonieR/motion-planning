%% Copyright (C) 2017 Neken Aritia
%% 
%% Author: Neken Aritia <Neken Aritia@LAPTOP-2DCVVNCP>
%% Created: 2017-04-26

function [tau,T,traj] = generateProfile (p,vel,r,L,kmin,Vmax,amax,wmax,emax,t0,tmin)
    mvt = p{1};
    %Dur�e de la phase nominale tNom, arrondi � k'*t0
    Vc=vel*Vmax;
    if mvt=='LIN'
        d=str2double(p{2});
    elseif mvt=='ROT'
        alpha=str2double(p{2});
        p{4}=sign(alpha);
        alpha=abs(alpha);
        d=alpha*(L/2);
    elseif mvt=='CIR'
        l=str2double(p{2});
        alpha=str2double(p{3});
        p{4}=sign(alpha);
        alpha=abs(alpha);
        d=alpha*(l+L/2);
        nu=(l-L/2)/(l+L/2);
    end

    T1=d/Vc;
    t1=Vc/amax;

    dPhi=d/(2*pi*r);
    Txy=dPhi/(wmax);
    txy=wmax/emax;

    T=max(T1,Txy);
    tau=max(t1,txy);

    if T<tau
        tau=sqrt(d/amax);
        T=tau;
    end
  
    if T<kmin*t0 || tau<kmin*t0 
      tau=kmin*t0;
      T=tau;
    end

    k1=round(tau/t0);
    tau=k1*t0;

    k2=round(T/t0);
    T=k2*t0;

    %Temps de fin de la phase nominale
    Tf=T+tau;

    %INITIALISATION
    t=0;
    w1=[];
    w2=[];
    s1=[];
    s2=[];
    V1=[];
    a=Vc/tau;

    %PHASE D'ACCELERATION
    while t < tau
    V1(end+1)=a*t;
    s1(end+1)=(a/2)*power(t,2);
    t=t+t0;
    end

    %PHASE NOMINALE
    sTau=(a/2)*power(tau,2);
    while t < T
    V1(end+1)=Vc;
    s1(end+1)=Vc*(t-tau)+sTau;
    t=t+t0;
    end

    %PHASE DE DECELERATION
    sT=Vc*(T-tau)+sTau+(a/2)*power(tau,2);
    while t < T+tau
    V1(end+1)=a*(T+tau-t);
    s1(end+1)=-(a/2)*power(T+tau-t,2)+sT;
    t=t+t0;
    end


    if mvt=='LIN'
    w1=V1/r;
    w2=w1;
    s2=s1;
    elseif mvt=='ROT'
    w1=p{4}*V1/r;
    w2=-w1;
    s2=-s1;
    elseif mvt=='CIR'
    w1=p{4}*V1/r;
    w2=nu*w1;
    s2=nu*s1;
    end
    traj = [s1' w1' s2' w2'];
end
