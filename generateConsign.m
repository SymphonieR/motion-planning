function [parameters, consigns, showDiscret]=generateConsign(X,Y,XLim,nPoints)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
    
    XY=[X,Y];
    removeOutValue();
    plot(XY(:,1),XY(:,2), 'Color', [0.3 0.3 0.3], 'linewidth', 1.1);

    discretizeXY();
    showDiscret = plot(XY(:,1),XY(:,2),'-o', 'Color', [255 128 0]/255, 'linewidth', 1.2);
    
    [parameters,consigns] = convertToConsigns();
    
    function removeOutValue
        %Enlever les points dont les coordonn�es d�passent les limites
        xyRemoved = [];
        for i=1:length(X)
            if (X(i)>=0 && X(i)<=1.01*XLim && Y(i)>=0 && Y(i)<=1.01*XLim)
                xyRemoved=[xyRemoved; XY(i,:)];
            end
        end
        XY=xyRemoved;
    end

    function discretizeXY
        %R�duit l'ensemble d'un point � un ensemble discret constitu� de
        %nPoints
        n = length(XY);
        dx = (n-1)/nPoints;

        xyDiscret = zeros(nPoints,2);
        for i=0:nPoints
            xyDiscret(i+1,:) =  XY(round(1+dx*i),:);
        end
        XY=xyDiscret([1:end-2,end],:);
    end
        
    function [parameters,consigns] = convertToConsigns
        %Traduit les points en consigne (somme de LIN et ROT)
        
        parameters={};
        consigns={};
        
        rot=zeros(nPoints-1);
        dist=zeros(nPoints-1);

        for i = 1:nPoints-1 
            [rot(i), dist(i)] = cart2pol(XY(i+1,1)-XY(i,1), XY(i+1,2)-XY(i,2));
        end

        dRot=zeros(nPoints-1);
        dRot(1)=rad2deg(rot(1)-pi/2);

        for i = 2:nPoints-1
            dRot(i) = (rot(i) - rot(i-1)); 
            if abs(dRot(i))>abs(2*pi-dRot(i))
                dRot(i)=dRot(i)-2*pi;
            end
            if abs(dRot(i))>abs(2*pi+dRot(i))
                dRot(i)=2*pi+dRot(i);
            end
            dRot(i)=rad2deg(dRot(i));
        end

        for i= 1:nPoints-1
            if dRot(i)~=0
                parameters{end+1} = {'ROT ',dRot(i)};
                consigns{end+1} = horzcat( 'ROT',' ',num2str(dRot(i)),'d');
            end
            parameters{end+1} = {'LIN ',dist(i)}; 
            consigns{end+1} = horzcat( 'LIN',' ',num2str(dist(i)),'m');
        end
    end

end

