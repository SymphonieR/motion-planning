function GUI_trajectory(cmd)
    % Interface graphique associ�e � la g�n�ration de trajectoire
    
    XLim=1;
    YLim=1;
    buttonColor = [59 190 192]/255;
    buttonTextColor = [1 1 1];
    barMenuColor = [49 155 161]/255;
    buttonTextFocus = [0 0.4 0.4];
    titleColor = [0.25 0.25 0.25];
    backgroundColor= [.94 .94 .94];
    
    %Si le nombre d'argument == 0 (au lancement du programme), la commande
    %est initialis�
    if nargin == 0
        cmd = 'init';
    end

    switch cmd
        case 'init'
            %A l'initialisation
            
            %Fenetre englobant toute l'interface
            fig = figure('DoubleBuffer','on','back','off','Resize', 'off',...
                'Name','G�n�ration de trajectoire', 'Position', [50 90 1440 650]);
            
            %% Zones d'affichage
            %Fenetre d'acquisition du dessin de la trajectoire
            info.ax = axes('XLim',[0 XLim],'YLim',[0 XLim],...
                'Box','on', 'Position',[0.205 0.09 0.4 0.845],...
                'XGrid', 'on', 'YGrid', 'on', 'color', [.94 .94 .94]);
            %Titre de la fenetre d'acquisition
            uicontrol('style','text','String',upper('Planification de la trajectoire'),...
                'Position', [296 608 577 25],'FontName', 'Roboto', 'FontSize',11,...
                'ForegroundColor', titleColor);
            
            %Fenetre affichant le profil de la consigne en vitesse gauche
            commandLeft = axes('XLim',[0 10],'YLim',[-10 10],...
                'Box','on', 'Position',[0.64 0.555 0.335 0.38],...,
                'XAxisLocation', 'origin', 'TickDir', 'both','color',[.94 .94 .94]);
            %Titre de la fenetre de consigne
            uicontrol('style','text','String',upper('Consigne moteur gauche'),...
                'Position', [970 608 400 25],'FontName', 'Roboto', 'FontSize',11,...
                'ForegroundColor', titleColor);
            %Abscisse de la fenetre de consigne
            uicontrol('style', 'text', 'String', 't(s)',...
                'Position',[1370 335 30 20],'Fontsize',10)
            %Ordonnee de la fenetre de consigne
            ylabel('rad/s')
            
            %Fenetre affichant le profil de la consigne en vitesse droite
            commandRight = axes('XLim',[0 10],'YLim',[-10 10],...
                'Box','on', 'Position',[0.64 0.091 0.335 0.38],...
                'XAxisLocation', 'origin', 'TickDir', 'both','color',[.94 .94 .94]);
            %Titre de la fenetre de consigne
            uicontrol('style','text','String',upper('Consigne moteur droit'),...
                'Position', [970 310 400 20],'FontName', 'Roboto', 'FontSize',11,...
                'ForegroundColor', titleColor);
            %Abscisse de la fenetre de consigne
            uicontrol('style', 'text', 'String', 't(s)',...
                'Position',[1370 35 30 20],'Fontsize',10)
            %Ordonnee de la fenetre de consigne
            ylabel('rad/s')
            
            uicontrol('Style', 'text', 'Position', [0 0 250 700], 'BackgroundColor', buttonColor);
            
            %% Boutons de menu
            % Lancement de la g�n�ration de trajectoire � partir des
            % consigns
            play = uicomponent('Style','toggle','FontName','Marlett', 'String','4', ...
                'FontSize', 17, 'Position',[35 570 56 40], 'Callback',@onProceed,... 
                'backgroundColor', buttonColor, 'ForegroundColor', buttonTextColor);

            % Reinitialisation de l'interface
            clear = uicomponent('Style','toggle', 'Parent',fig,'FontName','Marlett','String','r', ...
                'FontSize', 17,'Position',[97 570 56 40], 'Callback',@onClear,...
                'backgroundColor', buttonColor,'ForegroundColor', buttonTextColor);
            
            % Envoi de la consigne g�n�r�e � l'arduino, 
            % export = 0 -> Pas d'envoi (etat par d�faut)
            % export = 1 -> Envoi en cours
            export = uicomponent('Style','toggle', 'Parent',fig,'FontName','Marlett', 'String','7', ...
                'FontSize', 17, 'Position',[160 570 56 40], 'Callback',@onExport,...
                'backgroundColor', buttonColor, 'ForegroundColor', buttonTextColor);
            
            javaButtonDisplay(play,buttonTextColor,3);
            javaButtonDisplay(clear,buttonTextColor,3);
            javaButtonDisplay(export,buttonTextColor,3);
            % Passage en mode manuel(ou l'utilisateur rentre les consigns 
            % � la main) ou automatique, o� l'utilisateur dessine la
            % trajectoire souhait�e)
            % manualMode = 0 -> Mode automatique (etat par d�faut)
            % manualMode = 1 -> Mode manuel 

            %% Parametres
            % Mise � l'�chelle de la fenetre d'acquisition de la trajectoire 
            % � l'aide d'un slider allant de 1 (�tat par d�faut), � 10m
            scaleSlider = uicontrol('Style','slider', 'Min', 1, 'Max', 10, 'Value',...
                XLim, 'Position', [718 10 150 20], 'Callback', @onSlider);
            scaleDisplay = uicontrol('Style','text', 'Position', [620 10 100 20],...
                'String', ['Echelle : 1:', num2str(XLim)], 'FontSize', 10);
            
            % Acquisition du nombre de points de discr�tisation spatiale
            % entr�e par l'utilisateur
            % Par d�faut nPoints = 10
            uicontrol('style', 'text', 'String', 'N :', 'Position',[780 615 50 20],'Fontsize', 10);
            nPoints = uicontrol('Style', 'edit', 'String', 10,...
                'Position', [816 615 50 20], 'value',2,'Callback',@onUpdateNPoints);
            
            barMenu = uicontrol('Style', 'text', 'Position', [1080 7 200 25], 'BackgroundColor', buttonColor);
            javaButtonDisplay(barMenu, barMenuColor, 2);
            % Arret du mouvement ou pas entre des consignes consecutives
            % stopMvt = 0 -> Pas d'arret (etat initial)
            % stopMvt = 1 -> Arret
            stopMvt = uicontrol('Style', 'checkbox', 'String','Start-Stop',...
            'Position', [1100 10 80 20], 'Value', 0,'Callback', @onUpdateConsign, 'Fontsize',9,...
            'BackgroundColor', buttonColor);
            displaySuperposed = uicontrol('Style', 'checkbox', 'String','Superposes',...
            'Position', [1185 10 85 20], 'Value', 1,'Callback', @onProceed,...
            'Fontsize', 9, 'BackgroundColor', buttonColor);
        
            %% Zones de texte
            % Affichage de la liste des consignes
            consignDisplay = uicomponent('Style','listbox', 'Parent',fig, 'String',{}, ...
            'Min',1, 'Max',1, 'Value',1, ...
            'Position',[35 253 180 301], 'backgroundcolor', [1 1 1]);
            javaButtonDisplay(consignDisplay, buttonTextColor,0);
            
            % Acquisition de la consigne entr�e par l'utilisateur en mode
            % manuel
            writtenConsign = uicomponent('Style','edit', 'Parent',fig, 'enable', 'off', ...
            'Position',[36 198 179 38], 'BackgroundColor', barMenuColor, 'Foreground',...
            [1 1 1], 'Callback', @onWriteCommand);
           
            javaButtonDisplay(writtenConsign, [0 0 0],0);
            
            manualMode = uicomponent('Style','toggle', 'FontName','Arial Black','Fontsize', 10,'String','A', ...
            'Position',[39 202 30 30], 'Callback',@onManual, 'Value', 0,...
            'BackgroundColor', [0.93 0.93 0.93], 'ForegroundColor', barMenuColor);
            javaButtonDisplay(manualMode, barMenuColor, 2);
            
            % Fenetre de debuggage
            displayDebugger = uicomponent('Style','ListBox', 'Parent',fig, ...
            'Position',[35 60.5 180 120.5], 'BackgroundColor',...
            [0.4 0.4 0.4], 'ForegroundColor', [1 1 1], 'FontSize', 10, 'Value', 1);
            javaButtonDisplay(displayDebugger, buttonTextColor,0);
            %% Initialisation des variables
            % Liste des formes dessin�es
            info.drawing = [];
            % Liste des coordonn�es du curseur souris 
            info.x = [];
            info.y = [];
            info.mode = 0;
           
            % Vecteur pour la g�n�ration du profil
            t = []; %vecteur temporelle
            wb = []; %consigne en PWM
            w = []; %consigne en vitesse angulaire
            
            set(fig,'UserData',info,...
                    'WindowButtonDownFcn',[mfilename,' down'])

        case 'down'
            %Au clic souris: Pour �viter la superposition de diff�rentes
            %trajectoires non reli�s, nous reinitialisation l'affichage �
            %chaque clic sur la zone de dessin
            myname = mfilename; %chemin vers le dossier courant
            fig = gcbf; %lecture de la figure courante
            info = get(fig,'UserData'); ;%lecture de UserData contenant 
            %l'objet info
            if info.mode==0
            axes(info.ax);
            curpos = get(info.ax,'CurrentPoint'); %lecture de la position
            % de la souris
            info.x = curpos(1,1); %abscisse du curseur
            info.y = curpos(1,2); %ordonn�e du curseur
            Xlim = xlim(info.ax); %lecture de l'echelle
            if info.x >=Xlim(1) && info.x<=Xlim(2)...
                && info.y >=Xlim(1) && info.y<=Xlim(2)
                cla(info.ax) %reinitialisation de l'affichage lorsque le 
                %clic a lieu dans la zone d'acquisition du dessin
            end
            info.drawing = line(info.x,info.y,'Color',[82 100 100]/255,'linewidth',1.005);
            %Dessine une extremit� de la ligne au point actuel
            hold on;
            plot(curpos(1,1), curpos(1,2), '^','Color',[82 100 100]/255);
            %Marque le point actuel par un triangle (extrimit� de la
            %trajectoire)
            set(fig,'UserData',info,...
                    'WindowButtonMotionFcn',[myname,' move'],...
                    'WindowButtonUpFcn',[myname,' up'])
            end
        case 'move'
            % Tant que la souris n'est pas relach�, on dessine une
            % trajectoire
            fig = gcbf;%lecture de la figure courante
            info = get(fig,'UserData');%lecture de UserData contenant 
            %l'objet info
            curpos = get(info.ax,'CurrentPoint');%lecture de la position
            % de la souris
            info.x = [info.x;curpos(1,1)]; %Ajout de l'abscisse du point 
            % courant � la liste des abscisses
            info.y = [info.y;curpos(1,2)]; %Ajout de l'ordonn�e du point 
            % courant � la liste des ordonn�es
            if info.mode==0
                set(info.drawing,'XData',info.x,'YData',info.y)% mise � jour 
                % du trac�
            end
            set(fig,'UserData',info) %mise � jour de UserData 
            
        case 'up'
            fig = gcbf;%lecture de la figure courante
            info = get(fig,'UserData');;%lecture de UserData contenant 
            %l'objet info
            curpos = get(info.ax,'CurrentPoint');%lecture de la position
            % de la souris
            set(fig,'WindowButtonMotionFcn','',...
                    'WindowButtonUpFcn','')
            plot(curpos(1,1), curpos(1,2), '^','Color',[82 100 100]/255);
            %Marque le point actuel par un triangle (extrimit� de la
            %trajectoire)
            
    end
    
    function javaButtonDisplay(button,color,thickness)
        lineColor = java.awt.Color(color(1),color(2),color(3));
        newBorder = javax.swing.border.LineBorder(lineColor,thickness);
        jButton = findjobj(button);
        jButton.Border = newBorder;
        jButton.repaint;
    end

    function onClear(~,~)
        %Reinitialise l'interface
        set(clear, 'ForegroundColor', [0 0.3 0.3]);
        javaButtonDisplay(clear, [0 0.3 0.3], 3);
        %% Reinitialisation des fenetres graphique
        clearSketch();%Fenetre de dessin
        clearAxes(commandLeft); %Fenetre de consigne de vitesse
        clearAxes(commandRight); %Fenetre de mesure de vitesse
        
        %% Reinitialisation des zones de texte
        set(consignDisplay, 'String', {}); %Liste des consignes
        set(displayDebugger, 'String', ""); %Debugger
        
        %% Reinitialisation des variables
        info.x = []; %Liste des abscisses du trac�
        info.y = []; %Liste des coordonn�es du trac�
        set(fig, 'UserData', info); %Stockage dans UserData
        
        % Vecteur pour la g�n�ration du profil
        t = []; %vecteur temporelle
        wb = []; %consigne en PWM
        w = []; %consigne en vitesse angulaire
        pause(0.01)
        javaButtonDisplay(clear, [1 1 1], 3);
        set(clear, 'ForegroundColor', [1 1 1]);
        
    end
    
    function clearSketch
        %Reinitialisation de la fenetre de dessin
        cla(info.ax); %Reinitialisation
        set(info.ax, 'XLim', [0 XLim], 'YLim', [0 XLim]); %Mise a l'echelle
        hold off;
    end

    function clearAxes(ax)
        %Reinitialisation de l'axe ax et recentrage de l'axe des abscisses
        %au centre de la fenetre
        cla(ax); %Reinitialisation de l'axe
        legend('off'); %Suppression de la legende
        set(ax, 'XLim', [0 10], 'YLim', [-10 10], 'Box', 'on'); %Recentrage 
        %de l'affichage
    end

    function onSlider(~,~)
        %Modifie l'echelle de la fenetre de dessin
        XLim=floor(get(scaleSlider,'Value')); %Lecture de la valeur d�finie
        %par l'utilisateur
        set(scaleDisplay,'String',['Echelle : 1:' num2str(XLim)]); %Affichage
        %de la valeur de l'echelle
        set(info.ax, 'XLim', [0 XLim]); %Mise a l'echelle de l'axe des abscisses
        set(info.ax, 'YLim', [0 XLim]); %Mise a l'echelle de l'axe des ordonnees
    end

    function onManual(~,~)
        %Activation ou desactivation de la barMenure d'acquisition de la
        %commande manuel selon le choix du mode d'acuisition de la consigne
        if get(manualMode,'Value') %Si le mode manuel est selectionne
            set(manualMode, 'String', 'M');%Afficher 'M' (Manuel)sur le bouton
            set(writtenConsign, 'enable', 'on'); %Activer la barMenure d'acquisition 
            set(nPoints, 'String', '/', 'enable', 'off');
            set(scaleSlider, 'enable', 'off');
            cla(info.ax);
            info.mode=1;
            set(manualMode,'BackgroundColor', barMenuColor,'ForegroundColor',[1 1 1]);
            javaButtonDisplay(manualMode, [1 1 1],2);
            %manuel
        else %Si le mode automatique est selectionne
            set(manualMode, 'String', 'A'); %Afficher 'A' (Auto) sur le bouton
            set(writtenConsign, 'enable', 'off');%Desactiver la barMenure d'acquisition
            set(nPoints, 'String', '10', 'enable', 'on');
            set(scaleSlider,'enable', 'on');
            set(consignDisplay, 'String', {});
            set(manualMode,'BackgroundColor', [0.95 0.95 0.95], 'ForegroundColor', barMenuColor);
            javaButtonDisplay(manualMode, barMenuColor,2);
            clearSketch();
            info.mode=0;
        end
        set(fig, 'UserData', info);
        set(writtenConsign, 'String', "");%Effacer la consigne ecrite eventuellement
    end

    function onWriteCommand(~,~)
        %Lecture d'une consigne �crite manuellement, qui s'ajoute � la
        %liste des consignes 
        format = ["(LIN) ([+]?[0-9]+\.?[0-9]*)m?",...
            "(ROT) ([+-]?[0-9]+\.?[0-9]*)d?",...
            "(CIR) ([+]?[0-9]+\.?[0-9]*)m? ([+-]?[0-9]+\.?[0-9]*)d?"]; 
        %Format d'une consigne 
        key = get(gcf,'CurrentKey'); %Touche clavier pressee
        if(strcmp (key , 'return')) %Si la touche "entree" a ete pressee"
            consign  = get(writtenConsign, 'String'); %Lecture de la 
            %consigne rentree par l'utilisateur
            if cell2mat((regexp(consign, format)))%Si la consigne entree 
                %possede le bon format
                consigns = get(consignDisplay, 'String'); %Liste des consignes
                %deja entrees 
                consigns{end+1} = consign; %Ajout de la derni�re consigne entree
                set(consignDisplay, 'String', consigns); %Mise a jour de 
                %l'affichage avec la consigne rentree
                set(writtenConsign, 'String', ""); %Efface la barMenure d'acquisition
                onProceed(); %Mise a jour de l'affichage du profil consigne
            else %Si la consigne entree n'est pas au bon format
                set(displayDebugger, 'String', {'Erreur : Format',...
                    'LIN Distance_m','ROT Angle_d', 'CIR Rayon_m Angle_d'})
                %Affichage d'une erreur sur le debugger
            end
        end
    end

    function onProceed(~,~)
        cla(info.ax);
        clearAxes(commandLeft);
        clearAxes(commandRight);
        set(play, 'ForegroundColor', [0 0.3 0.3]);
        javaButtonDisplay(play,[0 0.3 0.3], 3);
        consigns = get(consignDisplay, 'String'); %Liste des consignes
        fig = gcbf; %Figure actuelle
        info = get(fig, 'UserData'); %Objet contenant les coordonn�es des
        %points du trac�s
        if ~isempty(consigns) && get(manualMode, 'Value') 
            axes(info.ax);
           
            drawTrajectory(consigns);
            xlim = get(info.ax, 'XLim');
            ylim = get(info.ax, 'YLim');
            set(info.ax, 'XLim', [xlim(1)-0.5, xlim(2)+0.5]);
            set(info.ax, 'YLim', [ylim(1)-0.1, ylim(2)+0.1]);
            set(info.ax,  'color', [.94 .94 .94]);
            title(upper('Planification de la trajectoire'), 'FontName', 'Roboto');
            grid on;
            set(displayDebugger, 'String', 'Planification manuelle - OK');
        end
        if ~isempty(info.x) && ~get(manualMode, 'Value')
            set(consignDisplay, 'String', "");
            cla(info.ax)
            axes(info.ax)
            hold on;
            [parameters, consigns, showDiscret]=generateConsign(info.x, info.y, XLim,...
                str2double(get(nPoints,'String')));
            hold off;
            set(consignDisplay, 'String', consigns);
            set(displayDebugger, 'String', 'Planification automatique - OK');
        end
        
        if ~isempty(consigns)
            [t, w, wb] = generateTrajectory(consigns, get(stopMvt,'Value'),...
                get(displaySuperposed,'Value'),commandLeft, commandRight);
            display(commandLeft, t, w);
            display(commandRight, t, w);
        end
        pause(0.01)
        javaButtonDisplay(play,[1 1 1], 3);
        set(play, 'ForegroundColor', [1 1 1]);
    end

    function onUpdateConsign(~,~)
        clearAxes(commandLeft);
        clearAxes(commandRight);
        onProceed();
    end

    function onUpdateNPoints(~,~)
        if ~isempty(get(info.ax, 'Children'))
            onProceed();
        end
    end
        
    function display(ax, t, w)
        axes('Position',get(ax,'Position'),'box','on','xtick',[],'ytick',[], 'color', [.94 .94 .94]);
        set(ax,'Box', 'off', 'color', 'none');
        axes(ax);
        ax.XAxisLocation = 'origin';
        ax.TickDir = 'both';
        xlim([0 t(end)*1.007]);
        ylim([min(min(0, min(w))) max(max(max(w),0))*1.4]);
    end

    function onExport(~,~)
        set(export, 'ForegroundColor', [0 0.3 0.3]);
        javaButtonDisplay(export, [0 0.3 0.3], 3);
        if get(export, 'value')
            if ~isempty(wb)
                sendCommand(wb, displayDebugger, export);
            else 
                set(displayDebugger, 'String', 'Erreur : Pas de commande');
                set(export, 'value', 0);
            end
        end
        pause(0.01);
        javaButtonDisplay(export, [1 1 1], 3);
        set(export, 'ForegroundColor', [1 1 1]);  
    end
end