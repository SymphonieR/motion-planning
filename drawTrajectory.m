function drawTrajectory( consigns )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    rho = 0;
    theta = 90;
    mvti0 = 'NULL';
    mvti1 = 'NULL';
    x = [0];
    y = [0];
    courbes = {};
    formatCommande = ["(LIN) ([+]?[0-9]+\.?[0-9]*)m?",...
        "(ROT) ([+-]?[0-9]+\.?[0-9]*)d?",...
        "(CIR) ([+]?[0-9]+\.?[0-9]*)m? ([+-]?[0-9]+\.?[0-9]*)d?"];
    
    for k=1:length(consigns) %lecture de chaque ligne du fichier tant que la ligne n'est pas vide
        parameters = regexp(consigns(k), formatCommande, 'tokens');
        iParam = (find(~cellfun(@isempty,parameters)));
        p = parameters{iParam}{:};
        mvti1 = p{1}; 
        pCopy=p(2:end);
        parameters=cellfun(@str2double,pCopy);
        switch(mvti1)
            case 'LIN'
                rho = parameters;
                [x1 y1]=pol2cart(deg2rad(theta),rho);
                x(end+1)=x(end)+round(x1,2);
                y(end+1)=y(end)+round(y1,2);
            case 'ROT'
                theta = theta + parameters;
                rho = 0;
                [x1 y1]=pol2cart(deg2rad(theta),rho);
                x(end+1)=x(end)+x1;
                y(end+1)=y(end)+y1;
            case 'CIR'
                rho = parameters(1)
                alpha = parameters(2)
                theta1 =theta-90;
                t = linspace(deg2rad(theta1),deg2rad(theta1+alpha),100);
                h=x(end)-rho*cos(deg2rad(theta1))
                k=y(end)-rho*sin(deg2rad(theta1))
                xCIR = rho*cos(t) + h;
                yCIR = rho*sin(t) + k;
                courbes{end+1} = plot(xCIR,yCIR,'Color', [255 128 0]/255, 'linewidth', 1.1);
                hold on;
                courbes{end+1} = plot(x,y,'Color', [255 128 0]/255, 'linewidth', 1.1,'marker', 'o');
                rho = 0;
                x=[xCIR(end)];
                y=[yCIR(end)];
                theta = theta+alpha;
        end
    end
    disp(x);
    disp(y);
    courbes{end+1} = plot(x,y,'Color', [255 128 0]/255, 'linewidth', 1.1,'marker', 'o');
    hold off;
end

