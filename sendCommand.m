function sendCommand(wb,displayWindow, exportToggle)
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here

    %% Ouverture du port
    try 
        arduino=serial('COM4','BaudRate',9600); % create serial communication object 
        fopen(arduino); % initiate arduino communication
        set(exportToggle, 'String', 'g', 'Fontsize', 13);
        
        displayText = {};
        displayFlow("Connexion Xbee effectu�")
        displayFlow("D�but d'envoi");
        displayFlow("--------------------------------");
        
        t0 = 0.1;
        %% Envoi trajectoire to XBee
        i=1;
        while(i<=length(wb) && get(exportToggle, 'value'))
            fprintf(arduino, '%d', wb(i,1));
            displayFlow(num2str(wb(i,1)));
            pause(t0);
            
            fprintf(arduino, '%d', wb(i,2));
            displayFlow(num2str(wb(i,2)));
            pause(t0);
            i=i+1;
        end
        if ~get(exportToggle, 'value')
            displayFlow("--------------------------------");
            displayFlow("Interruption de la connexion");
            displayFlow(strcat("Donn�es envoy�es: ", num2str(2*i)));
        else
            displayFlow("--------------------------------");
            displayFlow("Fin d'envoi");
        end
        %% Fermeture du port
        fclose(arduino); 
        set(exportToggle, 'String', '7', 'Fontsize', 15);
    catch
        set(displayWindow, 'String', {"Erreur :", "la connexion Xbee a �chou�"});
    end
       
    function displayFlow(message)
        displayText{end+1} = message;
        set(displayWindow, 'Value', length(displayText));
        set(displayWindow, 'String', displayText);
    end
end

