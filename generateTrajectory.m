function [t, w, wb] = generateTrajectory (consignes, stop, displaySuperposed,...
                            commandLeft, commandRight )
    %UNTITLED4 Summary of this function goes here
    %   Detailed explanation goes here
    hold(commandLeft);
    hold(commandRight);
    %% Caract�ristiques du robot 

    % Dimensions 
    r=40*10^(-3); %rayon en m
    L=30*10^(-3); %longueur en m

    % Performances
    vmax=1; %vmax en m/s
    amax=0.5; %acceleration max sp�cifi� par le PRIME
    kmin=4; %constante fixant la vitesse seuil pour un profil trapezoidal
    
    red=1/36; %Rapport de r�duction du moto-r�ducteur
    Nmax_m=10200; %vitesse max de rotation max du moteur en tr/min
    wmax_m=(2*pi/60)*Nmax_m; %vitesse angulaire max du moteur en rad/s
    wmax=wmax_m*red; %vitesse angulaire max du motor�ducteur (et donc de la roue)
    eps_max=12.5; %acceleration angulaire max du moteur

    %% Param�tres de simulation
    t0=10*10^(-3); %discreditation du temps;
    tmin=5; %temps minimale du mouvement en s
    eps_min=10; %distance max de lin�arisation du mouvement
    vel = 1; %commande vitesse
    p={}; %Parametres d�pendant du mouvement

    %% Description du mouvement 
    profile=[]; %profil du mouvement sous la forme [phi1(t) w1(t) phi2(t) w2(t)]

    tf=0; %Vitesse finale du mouvement
    ts0=0;
    ti0=0;
    ti1=0;
    i=1; % Indice du mvt en cours
    
    formatCommande = ["(LIN) ([+]?[0-9]+\.?[0-9]*)m?",...
            "(ROT) ([+-]?[0-9]+\.?[0-9]*)d?",...
            "(CIR) ([+]?[0-9]+\.?[0-9]*)m? ([+-]?[0-9]+\.?[0-9]*)d?"];
    legendLeft = [];
    legendRight = [];
    
    leftDisplay = [];
    rightDisplay = [];
    
    %% LECTURE DU FICHIER
    for k=1:length(consignes) %lecture de chaque ligne du fichier tant que la ligne n'est pas vide
        parameters = regexp(consignes(k), formatCommande, 'tokens');
        iParam = (find(~cellfun(@isempty,parameters)));
        p = parameters{iParam}{:};
        mvti1 = p{1};      
                
        %% GENERATION DE LA TRAJECTOIRE
        [ti1,Ti1,profilei1]=generateProfile(p,vel,r,L,kmin,vmax,amax,wmax,eps_max,t0,tmin); 
        %ti1 = instant de fin de la phase d'acc�l�ration
        %Ti1 = instant de fin de la phase nominale


        %% SUPERPOSITION DU MOUVEMENT
        %La superposition des profils n'a lieu que si les mvt i-1 et i sont
        %lin�aires, et que si la somme des distance < eps_min, pour i>1
         ni = length(profilei1);
         
         it1=int16(ti1/t0)+1;%instant de fin de la phase d'acc�l�ration du mvt i
          %en prenant pour origine le d�but du mvt i

        if i>1 && ~stop %&& strcmp(mvti0,'LIN') && strcmp(mvti1,'LIN')
            t = linspace(tf-ti0,tf-ti0+ti1+Ti1,ni);
            if displaySuperposed
               hkL=plot(commandLeft,t, profilei1(:,2),':', 'color', [1 0.698 0.4],...
                    'DisplayName','Profils superposes','linewidth',2);
               hkR=plot(commandRight,t, profilei1(:,4),':', 'color', [1 0.698 0.4],...
                    'DisplayName','Profils superposes','linewidth',2);
            end

            plot(commandLeft,[tf-ti0+ti1,tf-ti0+ti1],[0,profilei1(it1,2)],'k--');
            plot(commandLeft,[tf-ti0+Ti1,tf-ti0+Ti1],[0,profilei1(it1,2)],'k--');
            plot(commandRight,[tf-ti0+ti1,tf-ti0+ti1],[0,profilei1(it1,4)],'k--');
            plot(commandRight,[tf-ti0+Ti1,tf-ti0+Ti1],[0,profilei1(it1,4)],'k--');
           
            ts0=tf-ti0; %instant de fin de la phase nominale du mvt i-1
            %en prenant pour origine le d�but du mvt i=1
            ts1=ts0+ti1; %instant de fin de la phase d'acc�l�ration du mvt i
            %en prenant pour origine le d�but du mvt i=1
            is0=int16(ts0/t0)-1; %indice associ� � ts0
            is1=int16(ts1/t0); %indice associ� � ts1

            fi1=profilei1(it1,:); %valeur du profil i en t=ti1
            fi0=profile(is0,:); %valeur du profil i-1 en t=Ti0

            t=ts0; %On se place � la fin de la phase nominale du mvt i-1
            j=is0; %indice parcourant le tableau du profil total (superposition de tous les profils)
            while t<ts1 
            profile(j,:)=((fi1-fi0)/(ts1-ts0))*t+(fi1/ts1-fi0/ts0)/(1/ts1-1/ts0); 
            %lin�arisation du profil superpos�
            j=j+1; %incr�mentation de j
            t=t+t0; %incr�mentation du temps discret
            end

            tf=ts0;%temps de fin du profil total, sans le mvt i
            while it1<=length(profilei1) 
              profile(j,:)=profilei1(it1,:); %copie du profil i dans le profil total
              it1=it1+1;
              j=j+1;
            end
        else 
            t = linspace(tf,tf+ti1+Ti1,ni);
            if displaySuperposed
               hkL=plot(commandLeft,t, profilei1(:,2),':', 'color', [1 0.698 0.4],...
                    'DisplayName','Profils superposes','linewidth',2);
               hkR=plot(commandRight,t, profilei1(:,4),':', 'color', [1 0.698 0.4],...
                    'DisplayName','Profils superposes','linewidth',2);
            end

            plot(commandLeft,[tf+ti1,tf+ti1],[0,profilei1(it1,2)],'k--');
            plot(commandLeft,[tf+Ti1,tf+Ti1],[0,profilei1(it1,2)],'k--');
            plot(commandRight,[tf+ti1,tf+ti1],[0,profilei1(it1,4)],'k--');
            plot(commandRight,[tf+Ti1,tf+Ti1],[0,profilei1(it1,4)],'k--');

            profile=[profile;profilei1]; %Concat�nation par lignes de la liste des profils et du profil i en cours
        end
        tf=tf+ti1+Ti1;%Le temps de fin de mvt correspond � 
        %la somme du temps de fin d�fini pr�c�demment et du temps de fin du mvt i
        %Maj des param�tres
        ti0=ti1;
        i=i+1; %Incr�mentation de la commande en vitesse
    end

    %% Affichage des profils
    n = length(profile); %longueur total de la liste d�crivant le profil
    t=linspace(0,tf,n); %vecteur temps
    
    axes(commandRight);
    hL=plot(commandLeft,t, profile(:,2), 'color', [0.4 0.8 0], 'linewidth',2,...
        'DisplayName', 'Profil moteur gauche'); %affichage du profil de vitesse du moteur 1 
    if displaySuperposed
        legend([hL,hkL],'location','north','orientation','horizontal');
    else
        legend(hL,'location','north','orientation','horizontal','boxoff');
    end
    legend boxoff;
    hold off;
    
    axes(commandRight);
    hR=plot(t, profile(:,4), 'color', [0.4 0.8 0], 'linewidth',2,...
        'DisplayName', 'Profil moteur droit');  %affichage du profil de vitesse du moteur 2
    if displaySuperposed
        legend([hR,hkR],'location','north','orientation','horizontal');
    else
        legend(hR,'location','north','orientation','horizontal');
    end
    legend boxoff;
    hold off;
    
%     if displaySuperposed
%         legend(commandLeft,[hL,hi],'location','north','orientation','horizontal');
%         legend(commandRight,[hL,hi],'location','north','orientation','horizontal');
%     else
%         legend(commandLeft,[hL],'location','north','orientation','horizontal');
%         legend(commandRight,[hL],'location','north','orientation','horizontal');
%     end
    
    %% Transformation en format PWM

    wb1 = floor(127+127*profilei1(:,2)/wmax);
    %Passage en binaire de la commande du moteur 1
    wb2 = floor(127+127*profilei1(:,4)/wmax);
    %Passage en binaire de la commande du moteur 2 
    wb = [wb1 wb2];
    w = [profile(:,2) profile(:,4)];

end

